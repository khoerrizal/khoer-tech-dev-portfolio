import { workIcon } from "../../public"
import Image from 'next/image'
import Link from "next/link"


const Navbar: React.FC = () => {
    return (
        <nav className="fixed bg-[#EFF0F3] w-full h-[86px] flex flex-row font-PlusJakartaSans items-center justify-between px-14 z-50">
            <div className="flex items-end">
                <span className="font-semibold text-4xl text-[#001858]">KhoerTechDev</span>
                <span className="text-[#6246EA]">.Blog</span>
            </div>
            <ol className="flex flex-row font-bold text-xl text-[#2B2C34] items-center gap-4">
                <li className="w-full h-full hover:bg-[#6246EA] hover:text-white text-center p-2 cursor-pointer"><Link href={"#hero-section"}>Home</Link></li>
                <li className="w-full h-full hover:bg-[#6246EA] hover:text-white text-center p-2 cursor-pointer"><Link href={"#project-section"}>Projects</Link></li>
                <li className="w-full h-full hover:bg-[#6246EA] hover:text-white text-center p-2 cursor-pointer"><Link href={"#skill-section"}>Skills</Link></li>
                <li className="w-full h-full hover:bg-[#6246EA] hover:text-white text-center p-2 cursor-pointer"><Link href={"/blog"}>Blog</Link></li>
                <li>
                    <Link href={"#hireme-section"}>
                        <button className="bg-[#6246EA] rounded py-3 px-5 text-[white] flex xl:flex-row flex-col gap-3 items-center hover:shadow-2xl">
                            <Image src={workIcon} alt={"Work Icon"} className="w-6 h-6 "/>
                            <span className="w-32 ">Hire Me</span>
                        </button>
                    </Link>
                </li>
            </ol>
        </nav>
    )
}

export default Navbar