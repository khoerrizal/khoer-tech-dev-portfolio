import Head from 'next/head'
import Navbar from '../../molecules/Navbar'

export interface AppLayoutProps {
    title: string
    description: string
    children: React.ReactNode
}

const AppLayout: React.FC<AppLayoutProps> = ({ title, description, children }) => {
    return (
        <div className="flex flex-col ">
            <Head>
                <title>{title}</title>
                <meta name="description" content={description} />
                <script
                async
                src="https://cdn.byteboost.io/v1beta/repo/inspector/main.js"
                data-byteboost-key="7eff3e1a-19c3-40d8-b605-2326eb417ed8"
                ></script>
            </Head>

            <Navbar />

            <main className="min-h-screen max-w-[1366px] w-full flex justify-center mx-auto overflow-hidden">
                {children}
            </main>

        </div>
    )
}

export default AppLayout
