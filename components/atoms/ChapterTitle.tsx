interface titleProps {
    title: string
}

const ChapterTitle: React.FC<titleProps> = ({ title }) => {
    return (
        <div className="flex flex-row items-center gap-3">
                    <span className="text-2xl font-bold">{title}</span>
                    <div className="w-8 h-0.5 bg-black"/>
                </div>
    )
}

export default ChapterTitle