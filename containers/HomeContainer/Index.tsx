import HeroSection from "./components/HeroSection"
import HireMeSection from "./components/HireMeSection"
import ProjectSection from "./components/ProjectSection"
import SkillSection from "./components/SkillSection"

const HomeContainer: React.FC = () => {
    return (
        <div className="w-full">
            <HeroSection/>
            <ProjectSection/>
            <SkillSection/>
            <HireMeSection/>
        </div>
    )
}

export default HomeContainer