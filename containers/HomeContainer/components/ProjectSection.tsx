import ChapterTitle from "../../../components/atoms/ChapterTitle"
import { projectList } from "../constant/projectList"
import Image from 'next/image'
import { useState } from "react"

const ProjectSection: React.FC = () => {
    const [limit, setLimit] = useState<number>(1)

    const handleMoreProject = () => {
        setLimit(limit +1)
    }
    return (
        <section id="project-section" className='relative flex flex-col w-full py-12 px-28 pt-[86px]'>
            <ChapterTitle title={"Projects"}/>
            <div className="grid justify-center mt-8 xl:grid-cols-3 lg:grid-cols-2">
                {projectList.slice(0, limit*3).map((line, index) => (
                    <div key={index} className="flex flex-col gap-6 p-5 cursor-pointer hover:shadow-2xl rounded-xl">
                        <div className='h-[198px] w-[260px] rounded-xl'>
                            <Image src={line.image} alt={'Project Icon'} className={'h-[198px] w-[260px] rounded-xl'}/>
                        </div>
                        <div className="flex flex-col">
                            <span className="text-xl font-bold">{line.title}</span>
                            <span>{line.from} - {line.to}</span>
                        </div>
                    </div>
                ))}
            </div>
            <div className="flex items-center justify-center w-full mt-20">
                <button onClick={handleMoreProject} className="bg-[#6246EA] text-white py-5 px-6 font-bold text-xl rounded-lg">More Projects</button>
            </div>
        </section>
    )
}

export default ProjectSection