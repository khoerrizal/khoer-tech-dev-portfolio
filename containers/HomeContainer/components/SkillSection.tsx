import ChapterTitle from "../../../components/atoms/ChapterTitle"
import { skillList } from "../constant/skillList"
import Image from 'next/image'

const SkillSection: React.FC = () => {
    return (
        <section id="skill-section" className='relative flex flex-col w-full pt-[86px] pb-12 px-28 bg-[#EFF0F3]'>
            <ChapterTitle title={"Skills"}/>
            <div className="flex flex-row w-full gap-8 mt-12 overflow-x-scroll scroll">
                <div className="flex pb-4 flex-nowrap gap-9">
                    {skillList.map((line, index) => (
                        <div key={index} className="w-[220px] h-[280px] bg-white rounded-lg justify-center flex flex-col p-8 cursor-pointer hover:bg-[#6246EA] hover:shadow-2xl hover:text-white">
                            <Image src={line.icon} alt={"Skill Icon"} className="w-8 h-8"/>
                            <span className="text-2xl font-bold">{line.name}</span>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    )
}

export default SkillSection