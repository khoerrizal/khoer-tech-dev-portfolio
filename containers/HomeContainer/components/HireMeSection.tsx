import Image from "next/image"
import ChapterTitle from "../../../components/atoms/ChapterTitle"
import { gmailIcon, khoerProfilePicture, mapsIcon, waIcon } from "../../../public/image/home"

const HireMeSection: React.FC = () => {
    return (
        <section id="hireme-section" className='relative flex flex-col w-full py-12 px-28'>
            <ChapterTitle title={"Hire Me"}/>
            <div className="flex flex-row mt-12 justify-evenly">
                <div className="relative hidden lg:flex">
                    <Image src={khoerProfilePicture} alt={"Khoer Profile Picture"} className="rounded-[320px] object-cover w-80 h-80"/>
                </div>
                <div className="h-80 w-0.5 bg-[#2B2C34] hidden lg:flex"/>
                <div className="text-[#2B2C34] flex flex-col font-normal text-lg items-start">
                    <div className="flex flex-col text-xl font-extrabold lg:flex-row">
                        <span>Khoerurrizal, S.Kom </span>
                        <span>| Full Stack Developer</span>
                    </div>
                    <div className="flex flex-row justify-center">
                        <div className="relative">
                            <Image src={waIcon} alt={"Whatsapp Icon"} className="w-12 h-12"/>
                        </div>
                        <a target="_blank" href="https://wa.me/6288809040672"><span className="hover:text-[#6246EA] items-center justify-center h-full align-middle flex">+62 812-1784-1865</span></a>
                    </div>
                    <div className="flex flex-row justify-center">
                        <div className="relative">
                            <Image src={gmailIcon} alt={"Gmail Icon"} className="w-12 h-12"/>
                        </div>
                        <a target="_blank" href="mailto:kuroi.nichirin@gmail.com"><span className="hover:text-[#6246EA] items-center justify-center h-full align-middle flex">kuroi.kuruma@gmail.com</span></a>
                    </div>
                    <div className="flex flex-row justify-center">
                        <div className="relative">
                            <Image src={mapsIcon} alt={"Maps Icon"} className="w-12 h-12"/>
                        </div>
                        <span className="hover:text-[#6246EA] items-center justify-center h-full align-middle flex">Jakarta, Indonesia</span>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default HireMeSection
