import Image from 'next/image'
import Link from 'next/link'
import { developerIllustration, heroBackground, instagramIcon, linkedinIcon, tiktokIcon, twitterIcon } from "../../../public/image/home"

const HeroSection: React.FC = () => {
    return (
        <section id="hero-section" className='relative flex flex-col w-full h-[500px] pt-[86px]'>
            <div className='absolute z-0'>
                <Image src={heroBackground} className={'w-[1440px] h-[500px] -top-20 object-cover'} alt={'Background'}/>
            </div>
            <div className='z-10 flex flex-row items-center justify-between w-full h-full px-16'>
                <div className='flex flex-col'>
                    <div className='w-[471px] px-6'>
                        <span className='font-bold text-[64px] text-black'>Hi, I’m Khoer Full Stack dev</span>
                    </div>
                    <div className='flex flex-row text-[#9A9494] text-xl font-normal mt-7'>
                        <span className='py-5 px-3 min-w-[240px] border-l-4 border-[#2B2C34]'>Online Course Trainer</span>
                        <span className='py-5 px-3 min-w-[240px] border-l-4 border-[#2B2C34]'>Backend Developer</span>
                        <span className='py-5 px-3 min-w-[240px] border-l-4 border-[#2B2C34]'>Frontend Developer</span>
                    </div>
                    <div className='flex flex-row gap-16 mt-10'>
                        <a target="_blank" href="https://twitter.com/UltraTechDev" rel="noopener noreferrer">
                            <div className='h-[72px] w-[72px] cursor-pointer'>
                                <Image src={twitterIcon} alt={'Twitter Icon'} className={'h-[72px] w-[72px]'}/>
                            </div>
                        </a>
                        <a target="_blank" href="https://www.instagram.com/kuroinichirin/" rel="noopener noreferrer">
                            <div className='h-[72px] w-[72px] cursor-pointer'>
                                <Image src={instagramIcon} alt={'Instagram Icon'} className={'h-[72px] w-[72px]'}/>
                            </div>
                        </a>
                        <a target="_blank" href="https://www.linkedin.com/in/khoer-rizal/" rel="noopener noreferrer">
                            <div className='h-[72px] w-[72px] cursor-pointer'>
                                <Image src={linkedinIcon} alt={'Linkedin Icon'} className={'h-[72px] w-[72px]'}/>
                            </div>
                        </a>
                        <a target="_blank" href="https://www.tiktok.com/@kurokiri009" rel="noopener noreferrer">
                            <div className='h-[72px] w-[72px] cursor-pointer'>
                                <Image src={tiktokIcon} alt={'Tik Tok Icon'} className={'h-[72px] w-[72px]'}/>
                            </div>
                        </a>
                    </div>
                </div>
                <div className='w-[434px] h-[363px] relative xl:flex hidden'>
                    <Image src={developerIllustration} className={"w-[434px] h-[363px]"} alt={'Developer'} layout={"fill"}/>
                </div>
            </div>
        </section>
    )
}

export default HeroSection