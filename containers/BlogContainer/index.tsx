import Image from "next/image"
import { useEffect, useState } from "react"
import { fetchApi } from "../../utils/api"

interface postType {
    tags: string[],
    title: string,
    body: string
}

const BlogContainer: React.FC = () => {
    const [isLoading, setIsLoading] = useState(false)
    const [postList, setPostList] = useState<postType[]>([])

    const retrievePostList = async () => {
        const res = await fetchApi('api/post')
        setPostList(res)
    }

    useEffect(() => {
        retrievePostList()
    }, [])
    return (
        <div className="w-full">
            <div className="bg-white py-24 sm:py-32">
                <div className="mx-auto max-w-7xl px-6 lg:px-8">
                    <div className="mx-auto max-w-2xl lg:mx-0">
                    <h2 className="text-4xl font-semibold tracking-tight text-pretty text-gray-900 sm:text-5xl">Blogs samples</h2>
                    <p className="mt-2 text-lg/8 text-gray-600">This page is example how to integrage between FE and BE.</p>
                    </div>
                    <div className="mx-auto mt-10 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 border-t border-gray-200 pt-10 sm:mt-16 sm:pt-16 lg:mx-0 lg:max-w-none lg:grid-cols-3">
                        {postList.map(article => {
                            return (

                                <article className="flex max-w-xl flex-col items-start justify-between">
                                    <div className="flex items-center gap-x-4 text-xs">
                                        {article.tags.map(tag => {
                                            return (
                                                <a href="#" className="relative z-10 rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100">{tag}</a>
                                            )
                                        })}
                                    </div>
                                    <div className="group relative">
                                    <h3 className="mt-3 text-lg/6 font-semibold text-gray-900 group-hover:text-gray-600">
                                        <a href="#">
                                        <span className="absolute inset-0"></span>
                                        {article.title}
                                        </a>
                                    </h3>
                                    <p className="mt-5 line-clamp-3 text-sm/6 text-gray-600">{article.body}</p>
                                    </div>
                                    <div className="relative mt-8 flex items-center gap-x-4">
                                    <Image src="https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" className="size-10 rounded-full bg-gray-50" width={300} height={300}/>
                                    <div className="text-sm/6">
                                        <p className="font-semibold text-gray-900">
                                        <a href="#">
                                            <span className="absolute inset-0"></span>
                                            Michael Foster
                                        </a>
                                        </p>
                                        <p className="text-gray-600">Co-Founder / CTO</p>
                                    </div>
                                    </div>
                                </article>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BlogContainer