import heroBackground from './Hero Background.png'
import developerIllustration from './developer-illustration.png'
import twitterIcon from './TwitterIcon.png'
import instagramIcon from './InstagramIcon.png'
import linkedinIcon from './LinkedinIcon.png'
import tiktokIcon from './Tik TokIcon.png'
import ptParagon from './pt-paragon.jpg'
import nextjsIcon from './nextjs-original-wordmark.png'
import khoerProfilePicture from './khoer-profile-picture.jpg'
import gmailIcon from './Gmail.svg'
import waIcon from './Whatsapp.svg'
import mapsIcon from './maps-icon.png'

export {
    heroBackground,
    developerIllustration,
    twitterIcon,
    instagramIcon,
    linkedinIcon,
    tiktokIcon,
    ptParagon,
    nextjsIcon,
    khoerProfilePicture,
    gmailIcon,
    waIcon,
    mapsIcon
}