import axios, { AxiosError } from "axios"

interface APIOptions {
    method?: 'GET' | 'POST' | 'PATCH' | 'DELETE' | 'PUT',
    params?: {},
    payload?: {}
}

export const fetchApi = async (endpoint: string, options?: APIOptions) => {
    const token = localStorage.getItem("khoertechdev-app-token")
    try {
        const res = await axios({
            url: endpoint,
            method: options?.method || 'GET',
            params: options?.params,
            data: options?.payload,
            headers:{
                Authorization: `Bearer ${token}`
            }
        })
        return res.data
    } catch (error) {
        console.log(error)
        if(error instanceof AxiosError){
            throw Error(error?.response?.data?.message || 'Failed to get data')
        } else {
            console.log('Unexpected error', error)
        }
    }
}