export interface IProviderProps {
    children: JSX.Element
}

export interface IHomeState {
    status: boolean
    setStatus: (status:boolean) => void
}