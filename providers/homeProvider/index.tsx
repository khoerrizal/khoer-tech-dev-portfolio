import { createContext, useContext, useState } from "react"
import { IHomeState, IProviderProps } from "./types"

export function createCtx<IHomeState>() {
    const ctx = createContext<IHomeState | undefined>(undefined)
    function useCtx() {
        const c = useContext(ctx)
        if (!c) throw new Error('useCtx must be inside a Provider with a value')
        return c
    }
    return [useCtx, ctx.Provider] as const
}

export const [useHomeContext, CtxProvider] = createCtx<IHomeState>()

export const HomeProvider: React.FC<IProviderProps> = ({ children }) => {
    const [status, setStatus] = useState<boolean>(false)
    
    const state: IHomeState = {
        status,
        setStatus
    }

    return <CtxProvider value={state}>{children}</CtxProvider>
}