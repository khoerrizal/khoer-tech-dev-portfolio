export interface IProviderProps {
    children: JSX.Element
}

export interface IGlobalState {
    status: boolean
    setStatus: (status:boolean) => void
}