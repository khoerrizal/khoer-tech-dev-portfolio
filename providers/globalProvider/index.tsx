import { createContext, useContext, useState } from "react"
import { IGlobalState, IProviderProps } from "./types"

export function createCtx<IGlobalState>() {
    const ctx = createContext<IGlobalState | undefined>(undefined)
    function useCtx() {
        const c = useContext(ctx)
        if (!c) throw new Error('useCtx must be inside a Provider with a value')
        return c
    }
    return [useCtx, ctx.Provider] as const
}

export const [useGlobalContext, CtxProvider] = createCtx<IGlobalState>()

export const GlobalProvider: React.FC<IProviderProps> = ({ children }) => {
    const [status, setStatus] = useState<boolean>(false)
    
    const state: IGlobalState = {
        status,
        setStatus
    }

    return <CtxProvider value={state}>{children}</CtxProvider>
}