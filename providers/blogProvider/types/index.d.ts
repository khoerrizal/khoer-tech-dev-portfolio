export interface IProviderProps {
    children: JSX.Element
}

export interface IBlogState {
    status: boolean
    setStatus: (status:boolean) => void
}