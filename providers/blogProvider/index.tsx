import { createContext, useContext, useState } from "react"
import { IBlogState, IProviderProps } from "./types"

export function createCtx<IBlogState>() {
    const ctx = createContext<IBlogState | undefined>(undefined)
    function useCtx() {
        const c = useContext(ctx)
        if (!c) throw new Error('useCtx must be inside a Provider with a value')
        return c
    }
    return [useCtx, ctx.Provider] as const
}

export const [useBlogContext, CtxProvider] = createCtx<IBlogState>()

export const BlogProvider: React.FC<IProviderProps> = ({ children }) => {
    const [status, setStatus] = useState<boolean>(false)
    
    const state: IBlogState = {
        status,
        setStatus
    }

    return <CtxProvider value={state}>{children}</CtxProvider>
}