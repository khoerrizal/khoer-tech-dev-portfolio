import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import HomeContainer from '../containers/HomeContainer/Index'
import { HomeProvider } from '../providers/homeProvider'

const Home: NextPage = () => {
    const headList = {
        title: 'Home | Khoer Tech Dev',
        description: 'This page about home',
    }

    return (
        <HomeProvider>
            <AppLayout {...headList}>
                <HomeContainer />
            </AppLayout>
        </HomeProvider>
    )
}

export default Home
