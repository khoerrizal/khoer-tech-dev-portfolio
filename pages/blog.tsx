import type { NextPage } from 'next'
import AppLayout from '../components/templates/AppLayout'
import { BlogProvider } from '../providers/blogProvider'
import BlogContainer from '../containers/BlogContainer'

const Blog: NextPage = () => {
    const headList = {
        title: 'Blog | Khoer Tech Dev',
        description: 'This page is a demonstration of handling content management for example blogs.',
    }

    return (
        <BlogProvider>
            <AppLayout {...headList}>
                <BlogContainer />
            </AppLayout>
        </BlogProvider>
    )
}

export default Blog
