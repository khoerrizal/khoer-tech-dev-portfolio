import axios from "axios"

export default async function handler(req, res) {
    const BACKEND_URL = process.env.REACT_APP_BACKEND_URL
    const url = `${BACKEND_URL}/posts`
    const result = await axios(url)
    res.status(200).json(result.data.posts);
}